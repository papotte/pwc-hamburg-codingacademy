package de.pwc.codingacademy;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class ChatBotTest {

    private ChatBot cb = new ChatBot();

    @Test
    public void sayHelloBot() {
        String reply = cb.getReply("Hello Bot");
        assertThat(reply, is("Hello human."));
    }

    @Test
    void sayHowAreYouDoing() {
        String reply = cb.getReply("How are you doing?");
        assertThat(reply, is("I'm fine, and you?"));
    }

    @Test
    void sayTheDate() {
        String reply = cb.getReply("What day is it today?");
        assertThat(reply, is("Today is Friday."));
    }

    @Test
    void sayYouAreWelcome() {
        String reply = cb.getReply("Thank you");
        assertThat(reply, is("You're welcome!"));
    }
}