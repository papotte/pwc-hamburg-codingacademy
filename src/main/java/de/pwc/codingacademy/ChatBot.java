package de.pwc.codingacademy;

import com.rivescript.Config;
import com.rivescript.RiveScript;
import de.pwc.codingacademy.handlers.TodayHandler;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;

public class ChatBot {

    private RiveScript bot;
    private static final String RIVESCRIPT_DIR = "rivescript";
    public ChatBot() {
        bot = new RiveScript(Config.utf8());
        addSubroutines();
        try{
            bot.loadDirectory(getRiveScriptDir());
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        bot.sortReplies();
    }

    private void addSubroutines() {
        bot.setSubroutine("today", new TodayHandler());
    }

    private String getRiveScriptDir() throws FileNotFoundException{
        return ResourceUtils.getFile(RIVESCRIPT_DIR).getPath();
    }

    public String getReply(String message) {
        return bot.reply("user", message);
    }
}
