package de.pwc.codingacademy.rest;

import de.pwc.codingacademy.ChatBot;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostQuestionController {
    private ChatBot chatBot = new ChatBot();

    @PostMapping("/message")
    public ResponseEntity<String> postMessage(@RequestBody String message) {
        String reply = chatBot.getReply(message);
        return new ResponseEntity<>(reply, HttpStatus.OK);
    }
}
