package de.pwc.codingacademy.handlers;

import com.rivescript.macro.Subroutine;

import java.util.Date;

public class TodayHandler implements Subroutine {
    public String call (com.rivescript.RiveScript rs, String[] args) {
        Date date = new Date();
        int day = date.getDay();

        switch(day){
            case 0: return "Sunday";
            case 1: return "Monday";
            case 2: return "Tuesday";
            case 3: return "Wednesday";
            case 4: return "Thursday";
            case 5: return "Friday";
            case 6: return "Saturday";
        }
        return "";
    }
}
